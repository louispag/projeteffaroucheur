# coding=UTF-8
import json
import os

# paramêtre par défaut
program_folder = os.getcwd()

def readsettings():
    with open(os.getcwd() + "/settings.json", 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)
    
    program_folder = allsettings['program_folder']

    return program_folder


    return True

def init_settings(pathtosettings):
    
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)

    allsettings['program_folder'] = os.getcwd()
    with open(pathtosettings, 'w') as jsonsettings:
        json.dump(allsettings,jsonsettings, indent=4)

def init():
    try:
        pathtosettings = os.getcwd() + "/settings.json"
        init_settings(pathtosettings)
        program_folder = os.getcwd()
    except:
        print('error at initialisation')




