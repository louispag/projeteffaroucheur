import os
from multiprocessing import Pool, Process, Pipe, Queue
from time import sleep
import initialisation
from action_choice import choicemaker as cmk, settings as cmk_param, test_detect_generator as detectgen, pipesinit, detectionlog
import detection.predict as pred
import camera.fake_cam as cam


# récupération de la taille du buffer depuis les parametres
logsize = cmk_param.buffersize

# création de l'historique avec le buffer de detectionlog
historique = detectionlog.historique

# ouverture du PIPE de transfert de donnée entre les détections et la boucle de décision. 
# Doit envoyer des TUPLES de 3 éléments tels que : (STRING "time" , Boulean True si détection, FLOAT degré de certitude) par ex : ('152725', True, 0.92)
# detectionIN pour le sous-système de détection, detectionOUT pour le sous-systeme de choix
detectionIN, detectionOUT = Pipe(duplex=True)



if __name__ == '__main__':
    initialisation.init()
    camPredictQueue = Queue() #implémente aussi mécanismes de synchro
    detectionQueue = Queue()
    
    # script de prédiction
    prediction = Process(target= pred.predict_routine, args=(camPredictQueue, detectionQueue,))
    prediction.start()
    
    #script de camera
    camp = Process(target = cam.cam_routine, args=(camPredictQueue,))
    camp.start()

    #########################################################
    # # lancement de la boucle de choix d'action, de décision
    decisionboucle = Process(target=cmk.boucledecision,args=(detectionQueue,))
    decisionboucle.start()

    # fermeture de la boucle de décision
    decisionboucle.join()

    #cloture du script de prédiction ici ? TBC
    prediction.join()

    camp.join()
    