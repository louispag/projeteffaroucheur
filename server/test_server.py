# coding=UTF-8
import requestsmanager
import apimanager
from pathlib import Path
import settings

# initialisation des variables
applicationportnumber = settings.effaroucheurportnumber

effaroucheurID = '110011'
testpicturename = 'defaultpic.jpg'
testsfile_folder = Path('f:/GitHub/serveur/pictures')

picpath = str(testsfile_folder / Path(testpicturename))

#print(requestsmanager.sendalertboar(picpath).text)

apimanager.app.run(host= '0.0.0.0', port=int(applicationportnumber))