# coding=UTF-8
# ceci est le fichier de gestion des appels de communications de l'effaroucheur vers le serveur de gestion, la 'porte de sortie' de celui-ci
import pprint
# importation de flask comme serveur web
from flask import Flask, send_from_directory, request, abort, send_file
# utilisation de la librairie os pour gérer les chemins d'accès et la navigation dans les fichiers
import os
# importation de json pour pouvoir lire les fichiers json contenant les métadonnées
import json
# utilisation de la librairie PIL pour gérer les images
from PIL import Image as img
# utilisation de la librairie 'Path' pour assurer une bonne gestion des chemins de fichiers
from pathlib import Path
# importation de la librairie request pour réaliser des requêtes vers le serveur distant
import requests
import settings
from datetime import datetime
#from celery import Celery

#appcelery = Celery('tasks', broker='redis://localhost', backend='redis://localhost')

### importation des settings
pictures_folder = settings.pictures_folder
serversanglier_adresse = settings.serversanglier_adresse
serversanglier_port = settings.serversanglier_port
serversanglier_link = settings.serversanglier_link
effaroucheurID = settings.effaroucheurID

# importation de la liste des API
alert_sanglier_api = settings.alert_sanglier_api
alert_panne_api = settings.alert_panne_api
status_api = settings.status_api
photo_api = settings.photo_api

# fonction d'envoi d'une alerte SANGLIER - nécessite le nom de l'image en entrée
#@appcelery.task
def sendalertboar(picturename):  
    try:
        picpath = str(pictures_folder / Path(picturename))
        with open(picpath, 'rb') as img:
            picturefile = {'file': (picpath, img, 'multipart/form-data')}
            serversanglier_api = serversanglier_link + alert_sanglier_api
            serverresponse = requests.post(serversanglier_api, files=picturefile)
    except :
        serverresponse = None
    return serverresponse
####

# fonction d'envoi d'une alerte PANNE. Le paramètre est un dictionnaire décrivant la panne (en gros : un json)
#@appcelery.task
def send_alertfailure(failure = {"Type" : "Unknown problem"}):
    try:
        serverresponse = requests.post(serversanglier_link + alert_panne_api, data=failure)
    except:
        serverresponse = None
    return serverresponse
####

# fonction d'envoi du status de l'effaroucheur, 'status', la variable en entrée, est un dictionnaire décrivant le status de l'effaroucheur
#@appcelery.task
def send_status(status = {"Status" : "OK"}):
    try:
        serverresponse = requests.post(serversanglier_link + status_api, data=status)
    except:
        serverresponse = None
    return serverresponse
####

# fonction d'envoi d'une image issue de l'effaroucheur - nécessite le nom de l'image en entrée
#@appcelery.task
def send_picture(picturename):
    try:
        picpath = str(pictures_folder / Path(picturename))
        
        with open(picpath, 'rb') as img:
            picturefile = {'file': (picpath, img, 'multipart/form-data')}
            serversanglier_api = serversanglier_link + photo_api
            serverresponse = requests.post(serversanglier_api, files=picturefile)
    except:
        serverresponse = None
    return serverresponse
####
