# coding=UTF-8
import requestsmanager
import apimanager
from pathlib import Path
import settings
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("typeofmessage")
parser.add_argument("messageparameter")
args = parser.parse_args()

# probablement A SUPPRIMER 
# testpicturename = 'defaultpic.jpg'
# testsfile_folder = settings.pictures_folder

# picpath = str(testsfile_folder / Path(testpicturename))



# variables par dpythonéfaut, qui seront prise en argument
messagetype = args.typeofmessage
picturename = args.messageparameter
status = args.messageparameter


if messagetype == 'picture':
    print(requestsmanager.send_picture(picturename))

if messagetype == 'failure':
    requestsmanager.send_status(status)

