# coding=UTF-8
# ceci est le fichier de gestion des API de l'effaroucheur, la 'porte d'entrée' de celui-ci

import pprint
# importation de flask comme serveur web
from flask import Flask, send_from_directory, request, abort, send_file
# utilisation de la librairie os pour gérer les chemins d'accès et la navigation dans les fichiers
import os, glob
# importation de json pour pouvoir lire les fichiers json contenant les métadonnées
import json
# utilisation de la librairie PIL pour gérer les images
# from PIL import Image as img
# utilisation de la librairie 'Path' pour assurer une bonne gestion des chemins de fichiers
from pathlib import Path
import settings
import requestsmanager

program_folder = settings.program_folder

app = Flask(__name__)

# page d'accueil de l'effaroucheur
@app.route('/')
def mainpage():
    greetings = "Ceci est l'effaroucheur n°" + settings.effaroucheurID
    return greetings, 200

# API de demande de photo
@app.route('/photo', methods=['GET'])
def picturerequest():
    # ajouter appel de fonction de prise de photo qui retourne le nom de la picture
    nomphoto = trouvedernierephoto()

    os.system("python " + program_folder + "server/messagesender.py picture %s"%nomphoto)
    # picture = takepicture()
    # requestsmanager.send_picture(pictureID)
    return nomphoto, 200

# API de demande de status
@app.route('/status', methods=['GET'])
def statusrequest():
    status = {"Status" : "OK"}
    return status, 200


def trouvedernierephoto():
    nomphoto = max(glob.iglob(str(program_folder + settings.pictures_folder) + '/*'),key=os.path.getctime)


    return nomphoto
# configuration des dossiers nécessaires au bon fonctionnement de l'effaroucheur
# pictures_folder = settings.pictures_folder


