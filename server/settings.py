# coding=UTF-8
from pathlib import Path
import json
import os

### settings de tests
pictures_folder = './pictures'
serversanglier_adresse = '127.0.0.1'
serversanglier_port = '59595'
serversanglier_link = 'http://' + serversanglier_adresse + ':' + serversanglier_port
effaroucheurID = '000'
effaroucheurportnumber = '32125'


# liste des API
alert_sanglier_api = '/alerts/boar/' + effaroucheurID
alert_panne_api = '/alerts/failure/'+ effaroucheurID
status_api = '/effaroucheurs/status/'+ effaroucheurID
photo_api = '/effaroucheurs/photo/'+ effaroucheurID



def readsettings(pathtosettings):
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)
    
    pictures_folder = allsettings['dossier_images']
    serversanglier_adresse = allsettings['serversanglier_adresse']
    serversanglier_port = allsettings['serversanglier_port']
    effaroucheurID = allsettings['effaroucheurID']
    effaroucheurportnumber = allsettings['effaroucheurportnumber']
    program_folder = allsettings['program_folder']

    serversanglier_link = 'http://' + serversanglier_adresse + ':' + serversanglier_port
    # liste des API
    alert_sanglier_api = '/alerts/boar/' + effaroucheurID
    alert_panne_api = '/alerts/failure/'+ effaroucheurID
    status_api = '/effaroucheurs/status/'+ effaroucheurID
    photo_api = '/effaroucheurs/photo/'+ effaroucheurID

    return pictures_folder, serversanglier_adresse, serversanglier_port, effaroucheurID, effaroucheurportnumber, serversanglier_link, program_folder, alert_sanglier_api, alert_panne_api, status_api, photo_api

def writesettings(pathtosettings):
    
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)

    allsettings['dossier_images'] = pictures_folder
    allsettings['serversanglier_adresse'] = serversanglier_adresse
    allsettings['serversanglier_port'] = serversanglier_port
    allsettings['effaroucheurID'] = effaroucheurID
    allsettings['effaroucheurportnumber'] = effaroucheurportnumber

    with open(pathtosettings, 'w') as jsonsettings:
        json.dump(allsettings,jsonsettings)


    return True



try:
    pathtosettings = os.getcwd() + "/../settings.json"
    pictures_folder, serversanglier_adresse, serversanglier_port, effaroucheurID, effaroucheurportnumber, serversanglier_link, program_folder, alert_sanglier_api, alert_panne_api, status_api, photo_api = readsettings(pathtosettings)

except:
    pathtosettings = os.getcwd() + "/settings.json"
    pictures_folder, serversanglier_adresse, serversanglier_port, effaroucheurID, effaroucheurportnumber, serversanglier_link, program_folder, alert_sanglier_api, alert_panne_api, status_api, photo_api = readsettings(pathtosettings)



