#############################################################################################
#this python file aims to load the model and set all the environement to make predictions
#############################################################################################
import tensorflow as tf
from tensorflow import keras
import os 

MODEL_NAME = "MobileNetV_DROP_2emeEtapeNEW_/"
MODEL_PATH = os.getcwd() + "/detection/models/"

def initialisation_detection():
    model = keras.Sequential()
    try:
        print(MODEL_PATH + MODEL_NAME)
        model = keras.models.load_model(MODEL_PATH + MODEL_NAME)
    except IOError:
        #charger dans log
        print("error while loading the model")
        return False
    except:
        print("error while loading the model")
        return False
    else:
        print("le model est chargé correctement")
        print(model.summary()) #à printer dans un fichier de log
        #envoyer dans log
        return model
#model = keras.Sequential()
#model = keras.models.load_model("/home/pi/effa/projeteffaroucheur/detection/models/" + "MobileNetV_DROP_2emeEtapeNEW_/")
#print(model.summary())