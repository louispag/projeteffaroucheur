#############################################################################################
#this python file aims to predict on images from the camera module
############################################################################################
import tensorflow as tf
import numpy as np
from . import detection_init as di
from . import detection_functions as df
import datetime
import cv2
import os
import time

def predict_routine(camPredictQueue, detectionQueue, barrier):
    print("predict routine is starting")
    #initialisation du modele
    model = di.initialisation_detection()
    #print("rentre dans predict_routine")
    if model is False:
        #print("pas bien chargé le modèle")
        return False
    print("bien chargé")
    
    #barriere
    print("barrier prediction")
    barrier.wait()
    #routine de prédiction
    print("predict routine : while loop is starting")
    while True:
        imageFromCam = camPredictQueue.get()
        image = df.image_preparation(imageFromCam)
        image = np.expand_dims(image, axis=0)
        time1 = time.time()
        prediction = model.predict(image, verbose=0)
        time2 = time.time()
        timetopred = time2 - time1
        print("probabilité de présence de sanglier = " + str(prediction[0][0]) + '      time to pred(s): ' + str(timetopred))
        predi = False
        if prediction[0][0] > 0.5:
            predi = True
        now = datetime.datetime.now()
        tuplePredi = (now.strftime("%H%M%S"), predi, prediction[0][0])
        detectionQueue.put(tuplePredi)
    

