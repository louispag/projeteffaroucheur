import cv2 as cv2
from skimage import transform

def image_preparation(image):
    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #median = cv2.medianBlur(gray,3)
    #histo = cv2.equalizeHist(median)
    histo = 1./255 * image
    #resized = cv2.resize(image, (400, 400))    
    #print(resized)
    resized = transform.resize(histo, (400, 400, 3))
    
    return resized