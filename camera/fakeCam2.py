import numpy as np
from cv2 import cv2
import os
import time
from PIL.Image import Image as img
from PIL import Image
import numpy as np

VIDEO_PATH = os.getcwd() + "/camera/video10.mp4"
picturepath = './pictures/'

def crop_image(img):
    height, width, ch = img.shape
    if width == height:
        return img
    img = np.array(img)
    offset  = int(abs(height-width)/2)
    if width>height:
        img = img[:,offset:width-offset,:]
    else:
        img = img[offset:height-offset,:,:]
    return Image.fromarray(img)

def fake_cam_routine(camPredictQueue, barrier):
    print("starting fake_cam_routine")
    cap = cv2.VideoCapture(VIDEO_PATH)
    fps = cap.get(cv2.CAP_PROP_FPS)
    step = fps / 2
    print("fps:" + str(fps) + " step: " + str(step))
    frameNumber = 0
    imagename = 1
    print("barrier fakecam")
    barrier.wait()
    print("start reading video")
    while(cap.isOpened()):
        ret, frame = cap.read()
        cv2.waitKey(1)
        try:
            picture = Image.fromarray(frame)
            picture.save(os.getcwd() + "/pictures/" + str(imagename) + '.jpg', "JPEG")
        except:
            pass

        imagename = (1 + imagename)%5
        if frameNumber == step:
            while camPredictQueue.empty() == False:
                camPredictQueue.get()
            cropped = crop_image(frame)
            resized = cv2.resize(np.float32(cropped), (400,400))
            cv2.imwrite(os.getcwd() + "/camera/forFakeCam.jpeg", resized)
            img = cv2.imread(os.getcwd() + "/camera/forFakeCam.jpeg")
            # affichage des images envoyees dans le pipeline de classification
            cv2.imshow('frame', img)
            camPredictQueue.put(img)
            frameNumber = 1
            time.sleep(0.2) #impératif sinon lock
        else:
            frameNumber += 1
            
    cap.release()
    cv2.destroyAllWindows()
    print("VIDEO TERMINEE")