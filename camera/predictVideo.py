from cv2 import cv2
from tensorflow import keras
import numpy as np
from PIL import Image
from skimage import transform
import glob
import os 
import time

def crop_image(img):
    height, width, ch = img.shape
    if width == height:
        return img
    img = np.array(img)
    offset  = int(abs(height-width)/2)
    if width>height:
        img = img[:,offset:width-offset,:]
    else:
        img = img[offset:height-offset,:,:]
    return Image.fromarray(img)

MODEL_PATH = "/home/louis/projeteffag/projeteffaroucheur/detection/models/MobileNetV_DROP_2emeEtapeNEW_/"
VIDEO_PATH = "/home/louis/projeteffag/projeteffaroucheur/camera/video12.mp4"

model = keras.models.load_model(MODEL_PATH)
print(model.summary())

cap = cv2.VideoCapture(VIDEO_PATH)
fps = cap.get(cv2.CAP_PROP_FPS)
step = fps / 2
print("fps:" + str(fps) + " step: " + str(step))
frameNumber = 0
globframe = 0
while(cap.isOpened()):
    ret, frame = cap.read()
    #cv2.imshow('frame', frame)
    cv2.waitKey(1)
    if frameNumber == step:
        frameNumber = 1
        cropped = crop_image(frame)
        resized = cv2.resize(np.float32(cropped), (400,400))
        cv2.imwrite("/home/louis/projeteffag/projeteffaroucheur/camera/pred50/test.jpeg", resized)
        img = cv2.imread("/home/louis/projeteffag/projeteffaroucheur/camera/pred50/test.jpeg")
        cv2.imshow('frame', img)
        transformed = transform.resize(img, (400, 400, 3))
        expended = np.expand_dims(transformed, axis=0)
        pred = model.predict(expended, verbose = 0)
        print(pred[0][0])
        if pred[0][0] > 0.50:
            globframe += 1
            cv2.imwrite("/home/louis/projeteffag/projeteffaroucheur/camera/pred50/" + str(globframe) + ".jpeg", resized)
        time.sleep(0.1) #impératif sinon lock
    else:
        frameNumber += 1
        
cap.release()
cv2.destroyAllWindows()
print("VIDEO TERMINEE")