import time
# import picamera
import numpy as np
from PIL import Image as img
import settings
import glob, os

def trouvedernierephoto(pictures_folder):
    program_folder = settings.program_folder
    nomphoto = max(glob.iglob(str(program_folder + pictures_folder) + '\*'),key=os.path.getctime)
    return nomphoto

def cam_routine(camPredictQueue):
    
    print("starting fake_cam_routine")
    # with picamera.PiCamera() as camera:
    #     camera.resolution = (1000, 1000)
    #     camera.framerate = 5
    pictures_folder = settings.program_folder + settings.pictures_folder
    output = np.empty((1024, 1008, 3), dtype=np.uint8)
    while(True):
        time.sleep(0.5)
        # camera.capture(output, 'rgb')

        picturename = trouvedernierephoto('/pictures')
        
        picpath = str(pictures_folder + picturename)
        
        with img.open(picturename) as image:
            while camPredictQueue.empty() == False:
                camPredictQueue.get()
                output = image.resize(1024,1008)
                camPredictQueue.put(output)


        