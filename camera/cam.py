import time
import picamera
import numpy as np
from PIL.Image import Image as img
from PIL import Image

picturepath = './pictures/'

def cam_routine(camPredictQueue, barrier):
    print("starting cam_routine")
    with picamera.PiCamera() as camera:
        camera.resolution = (1000, 1000)
        camera.framerate = 25
        output = np.empty((1024, 1008, 3), dtype=np.uint8)
        imagename = 1
        print("barrier camera")
        barrier.wait()
        print("cam routine : while loop is starting")
        while(True):
            time.sleep(0.5)
            camera.capture(output, 'rgb')
            while camPredictQueue.empty() == False:
                camPredictQueue.get()

            imagename = (1 + imagename)%5
            # code pour sauvergarder les photos sur le disque, dans ./pictures
            frame = output
            try:
                picture = Image.fromarray(frame)
                picture.save(str(picturepath) + str(imagename) + '.jpg', "JPEG")
            except:
                pass
            
            camPredictQueue.put(output)
