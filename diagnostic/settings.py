# coding=UTF-8
import json
import os

# paramêtre par défaut
program_folder = os.getcwd()
pathtosettings = os.getcwd() + "/settings.json"
def readsettings(pathtosettings = pathtosettings):
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)
    
    pictures_folder = allsettings['dossier_images']
    program_folder = allsettings['program_folder']

    return pictures_folder, program_folder




try:
    pathtosettings = os.getcwd() + "/../settings.json"
    pictures_folder, program_folder = readsettings(pathtosettings)

except:
    pathtosettings = os.getcwd() + "/settings.json"
    pictures_folder, program_folder = readsettings(pathtosettings)