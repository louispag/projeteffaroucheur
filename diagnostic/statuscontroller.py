import psutil
import settings
import glob
import os
import time
import datetime
import json

# check liaison avec serveur (utile ?)

# check fonctionnement du pipe (possible ?)

# check énergie (ingérable sans circuit physique - donc pour mémoire)
def checkenergy():
    status = True
    alert = None
    try :
        battery = psutil.sensors_battery()
        if battery == None:
            # ajouter fonction mise à jour status
            status = True
            alert += 'No battery info'
        else:
            charge = battery[0]
            timeleft = battery[1]
            plugged = battery[2]
            if charge < 20:
                status = False
                alert += 'Battery discharged'
            if plugged:
                status = True
                alert += None
            else:
                print('on battery')
                status = False
                alert += 'On battery'
    except:
        pass
    
    return status, alert


# fct de verification de présence de photo 'à jour' dans le dossier ./pictures retourne TRUE si camera OK / FALSE sinon
def checkCamera(periode):
    ok = False

    try:
        photo_path = max(glob.iglob(str(settings.program_folder + settings.pictures_folder) + '/*'),key=os.path.getctime)
        picture_time = os.path.getctime(photo_path)
        time_now = time.time()
        delta_time = time_now - picture_time
        if delta_time < periode:
            ok = True
    except:
        ok = False
    
    return ok
    
# fonction de sauvegarde du status de l'effaroucheur en .json
def sauvegarder_status(status):
    with open(settings.program_folder + "/status.json", 'w') as jsonsettings:
        json.dump(status,jsonsettings, indent=4)
    return True


# routine de controle du status de l'effaroucheur - vérifie que les signes vitaux sont OK 
# périodicité des vérifications (en secondes) en parametre, 60sec par défaut
def routine_control_status(barrier, periode=60): 
    print("starting status controler")
    # dictionnaire compilant l'ensemble du status
    status = {}
    #booléen donnant le status TRUE si tout marche
    status_global = True
    #time.sleep(1)
    print("barrier status controler")
    barrier.wait()
    # lancement de la boucle des vérifications
    print("control routine : while loop is starting")
    while True:
        status_global = True
        status['timestamp'] = str(datetime.datetime.now())

        # check le fonctionnement de l'acquisition d'image
        status['camera'] = checkCamera(periode//2)
        status_global = status_global and status['camera']
        
        # check de l'energie
        status['energy'], energy_pb = checkenergy()
        status_global = status_global and status['energy']
        
        # sauvegarde du status dans le dossier specifique & vérification accès disque
        try:
            status['disk'] = sauvegarder_status(status)
        except:
            status['disk'] = False
        status_global = status_global and status['disk']

        # action si probleme : envoyer une alerte panne avec le status
        if not status_global:
            # envoie une alerte de panne vers le serveur IoT
            try:
                os.system("python3 " + settings.program_folder + "/server/alertsender.py failure " + '"' + str(status) + '"' )
                print("Alerte panne envoyée")
                time.sleep(60)
            except:
                pass
        # pause avant prochaine boucle de vérification
        time.sleep(periode)

