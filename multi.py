import os
from multiprocessing import Pool, Process, Pipe, Queue, Barrier
from time import sleep
import initialisation
from action_choice import choicemaker as cmk, settings as cmk_param, test_detect_generator as detectgen, pipesinit, detectionlog
import detection.predict as pred
import diagnostic.statuscontroller as diag
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

FAKE_CAMERA = True
if FAKE_CAMERA:
    import camera.fakeCam2 as fakecam
else:
    import camera.cam as cam


# récupération de la taille du buffer depuis les parametres
logsize = cmk_param.buffersize

# création de l'historique avec le buffer de detectionlog
historique = detectionlog.historique

# ouverture du PIPE de transfert de donnée entre les détections et la boucle de décision. 
# Doit envoyer des TUPLES de 3 éléments tels que : (STRING "time" , Boulean True si détection, FLOAT degré de certitude) par ex : ('152725', True, 0.92)
# detectionIN pour le sous-système de détection, detectionOUT pour le sous-systeme de choix
detectionIN, detectionOUT = Pipe(duplex=True)


if __name__ == '__main__':
    initialisation.init()
    camPredictQueue = Queue() #implémente aussi mécanismes de synchro
    detectionQueue = Queue()
    barrier = Barrier(4)
    
    # creation du dossier d'image s'il n'est pas déjà créé
    try:
        os.mkdir("./pictures")
    except:
        pass
    # lancement du script d'acquisition vidéo
    if FAKE_CAMERA == True:
        camp = Process(target = fakecam.fake_cam_routine, args=(camPredictQueue, barrier,))
        camp.start()    
    else:
        camp = Process(target = cam.cam_routine, args=(camPredictQueue, barrier,))
        camp.start()
    
    # lancement du script de prédiction
    prediction = Process(target= pred.predict_routine, args=(camPredictQueue, detectionQueue, barrier,))
    prediction.start()     

    #########################################################
    # # lancement de la boucle de choix d'action & de décision
    decisionboucle = Process(target=cmk.boucledecision,args=(detectionQueue, barrier,))
    decisionboucle.start()

    # lancement de la boucle d'autodiagnostique
    autodiagnostique = Process(target=diag.routine_control_status,args=(barrier,60,))
    autodiagnostique.start()

    # fermeture de la boucle de diagnostique
    autodiagnostique.join()

    # fermeture de la boucle de décision
    decisionboucle.join()

    # cloture du script de prédiction
    prediction.join()

    # cloture de la routine d'acquisition vidéo
    camp.join()
    