# Actions d'effarouchement
# coding=UTF-8
from random import randint
from pathlib import Path
import os
from time import sleep
from playsound import playsound

    # action d'effarouchement "sonore" : pour l'instant simple, possible de faire mieux pour un véritable effaroucheur, dans l'avenir
    
def effarouchement_alarm_sound():
	# print("started effarouchement function")
	# print("work to do")
    try:
        playsound(os.getcwd() + '/../effarouch/explosion.mp3')
    except:
        playsound(os.getcwd() + '/effarouch/explosion.mp3')

if __name__ == '__main__':
    effarouchement_alarm_sound()
