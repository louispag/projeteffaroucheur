# script de choix d'action
# coding=UTF-8
import detectionlog
import os
from multiprocessing import Pool
import sys
from pipesinit import detectionIN
from time import sleep
import test_detect_generator
import actions
import settings
from multiprocessing import Process

#Default Values
logsize = 1200
probacourt = 0.5
probalong = 0.5
#

# fonction de test, de chargement d'un historique bidon
def test_chargemementhistorique(historique, logsize = 1200):
    for i in range(logsize):
        historique.enqueue(test_detect_generator.detect())
    
    return historique


def boucledecision(detectionQueue, barrier):
    # chargement des parametres depuis settings.py
    logsize = settings.buffersize
    probacourt = settings.probacourt
    probalong = settings.probalong
    print("starting boucledecision")
    boarherenow = False
    boarwashere = False
    nomphoto = 'default'
    # création de l'historique avec le buffer de detectionlog
    historique = detectionlog.CircularBuffer(logsize)
    tic = 1
    print("barrier choice maker")
    barrier.wait()
    # lancement de la boucle de décision d'action par analyse des détections
    print("choice maker : while loop is starting")
    while True:
        ### d'abord, ajout de la détection (ajouter PIPE depuis module detection) ### A CHANGER
        probapresencenow = detectionQueue.get()
        detection_now = probapresencenow
        historique.enqueue(detection_now)

        ### determination si un ou des sangliers sont ici maintenant
        boarherenow = areboarsherenow(historique, logsize)
        
        ### determination si un ou des sangliers sont ici sur la période
        if tic == 0:
            boarherelongtime = areboarshere(historique, logsize)
            # lance une alerte de présence sanglier si un ou des sanglier(s) sont présents trop longtemps
            if boarherelongtime:
                Process(target=actions.envoyeralertesanglier).start()

        # lance une action d'effarouchement si animal présent avec haute proba à l'instant
        if boarforsure(probapresencenow[2]):
            Process(target=actions.fairepeur).start()

        # lance une action d'effarouchement si animal présent avec proba moyenne, assez longtemps
        if boarherenow:
            Process(target=actions.fairepeur).start()

        tic = (tic + 1)%20



def boarforsure(probasanglier):
    boar = False

    if probasanglier > 0.9:
        boar = True
    return boar

# fonction de détection 'long terme' d'un sanglier
def areboarshere(historique, logsize):
    boarhere = False
    boarpresenceNow = 0.0
    for i in range(logsize):
        try:
            boardetected = historique.niemvalue(i)
            if boardetected[1]:
                boarpresenceNow += boardetected[2]
        except:
            pass
    
    boarproba = boarpresenceNow/float(logsize)
    if boarproba > probalong:
        boarhere = True
    return boarhere


# fonction de détection 'court terme' d'un sanglier
def areboarsherenow(historique, logsize):
    boarhere = False
    periodinterest = int(logsize / 10)
    boarpresencePeriod = 0.0
    for i in range(periodinterest):
        try:
            boardetected = historique.niemvalue(i)
            if boardetected[1]:
                boarpresencePeriod += boardetected[2]
        except:
            pass
    
    boarproba = boarpresencePeriod/float(periodinterest)
    
    if boarproba > probacourt:
        boarhere = True
    
    return boarhere

