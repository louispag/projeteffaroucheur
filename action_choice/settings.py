# coding=UTF-8
import json
import os

# paramêtre par défaut
probacourt = 0.5
probalong = 0.5
buffersize = 1200
program_folder = os.getcwd()
pathtosettings = os.getcwd() + "/settings.json"
def readsettings(pathtosettings = pathtosettings):
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)
    
    probacourt = allsettings['proba_presence_sanglier']
    probalong = allsettings['proba_sanglier_longterme']
    buffersize = allsettings['taille_historique_detection']
    pictures_folder = allsettings['dossier_images']
    program_folder = allsettings['program_folder']

    return probacourt, probalong, buffersize, pictures_folder, program_folder


## utile seulement pour créer correctement le fichier settings.json
def writesettings(pathtosettings = pathtosettings):
    
    with open(pathtosettings, 'r') as jsonsettings:
        allsettings = json.load(jsonsettings)

    allsettings['proba_presence_sanglier'] = probacourt
    allsettings['proba_sanglier_longterme'] = probalong
    allsettings['taille_historique_detection'] = buffersize
    with open(pathtosettings, 'r', 'w') as jsonsettings:
        json.dump(allsettings,jsonsettings, indent=4)

    return True

try:
    pathtosettings = os.getcwd() + "/../settings.json"
    probacourt, probalong, buffersize, pictures_folder, program_folder = readsettings(pathtosettings)

except:
    pathtosettings = os.getcwd() + "/settings.json"
    probacourt, probalong, buffersize, pictures_folder, program_folder = readsettings(pathtosettings)