# script de choix d'action
# coding=UTF-8
# import detectionlog
# import test_detect_generator
from time import sleep
from celery import group
#from test_detect_generator import detectchain
#from test_detect_generator02    import lotsofa
import os
from multiprocessing import Pool, Process

testgroup = ('test_detect_generator.py','choicemaker.py')

def run_test(testgroup):
    os.system('python {}'.format(testgroup))

if __name__ == '__main__':
    pool = Pool(processes=2)
    pool.map(run_test, testgroup)