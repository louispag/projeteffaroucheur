
import os
from multiprocessing import Pool, Process, Pipe, Manager
from time import sleep
import subprocess
from action_choice import choicemaker as cmk, settings as cmk_param, test_detect_generator as detectgen, pipesinit, detectionlog
# from pipesinit import detectionOUT, detectionIN

# création du buffer 
logsize = cmk_param.buffersize

# création de l'historique avec le buffer de detectionlog
historique = detectionlog.historique

detectionIN, detectionOUT = Pipe(duplex=True)

if __name__ == '__main__':
    
    historique = cmk.test_chargemementhistorique(historique, logsize)
    print(historique)

#     # Create process pool with four processes
#     num_processes = 2
#     pool = Pool(processes=num_processes)
#     processes = []
#     process_list = []
#     # Define IPC manager, pour gérer les IN et OUT entre les processus
#     manager = Manager()

#     # Define a list (queue) for tasks and computation results -> pour la com entre les processus lancés
#     tasks = manager.Queue()
#     results = manager.Queue()

#     # mise des processus dans la liste des processus
#     process_list.append(detectgen.detectchain,(1000, detectionIN,0))
#     process_list.append(cmk.boucledecision,(historique, detectionOUT))

# # Initiate the worker processes
#     for i in range(num_processes):

#         # Set process name
#         process_name = 'P%i' % i

#         # Create the process, and connect it to the worker function
#         new_process = Process(target=processes[i], args=(process_name,tasks,results))

#         # Add new process to the list of processes
#         processes.append(new_process)

#         # Start the process
#         new_process.start()

    # pour le test, le temps de mettre en place le pipe : création de l'historique

    

    # # lancement du processus de détection factice
    detectionboucle = Process(target=detectgen.detectchain,args=(1000, detectionIN,0))
    detectionboucle.start()
    # # lancement de la boucle de choix d'action
    decisionboucle = Process(target=cmk.boucledecision,args=(historique, detectionOUT))
    decisionboucle.start()

    
    decisionboucle.join()
    detectionboucle.join()
    

